#!/usr/bin/env node

const program = require('commander');
const bluebird = require('bluebird');
const os = require('os');
const path = require('path');
const mkdirp = bluebird.promisify(require('mkdirp'));
const glob = bluebird.promisify(require('glob'));
const fs = require('fs').promises;

let execute;
let options;

program
  .command('gen-ts')
  .option('-i, --in <dir>', 'Directory with input .ets files', '.')
  .option('--out-ts <dir>', 'Directory to output .ts files to')
  .option('--out-ts-map <dir>', 'Directory to output ets.ts.map files to')
  .action(opt => {
    execute = genTs;
    options = opt;
  });

program
  .command('gen-ejs')
  .option('-i, --in <dir>', 'Directory with input .js files', '.')
  .option('--in-ts-map <dir>', 'Directory with input ets.ts.map files')
  .option('--out-ejs <dir>', 'Directory to output .ejs files to')
  .action(opt => {
    execute = genEjs;
    options = opt;
  });

program
  .command('clean-up')
  .option('-i, --in <dir>', 'Directory with temporary files (.ets.ts, ets.ts.map)', '.')
  .action(opt => {
    execute = cleanUp;
    options = opt;
  });

program.parse(process.argv);

async function genTs() {
  options.outTs = options.outTs || options.in;
  options.outTsMap = options.outTsMap || options.outTs;

  const files = await glob(path.join(options.in, '**/*.ets'));

  for (const file of files) {
    await transpileFileTs(file);
  }
}

const SEPARATION_MARK = '/* template */';
const EJS_REGEX = /(<%%|%%>|<%=|<%-|<%_|<%#|<%|%>|-%>|_%>)/g;

async function ensureDir(file) {
  const dir = path.dirname(file);
  await mkdirp(dir);
}

async function transpileFileTs(file) {
  const pathInDir = path.relative(options.in, file);
  const content = await fs.readFile(file, 'utf8');

  const endOfTsIndex = content.indexOf(SEPARATION_MARK);

  const ejsContent = content.substring(endOfTsIndex + SEPARATION_MARK.length).replace(/^\r?\n?/, '');
  const tsContent = content.substring(0, endOfTsIndex).trimRight();

  let newContent = ejsContent.replace(EJS_REGEX, substring => {
    if (substring.startsWith('<%')) {
      return substring + '***/';
    } else if (substring.endsWith('%>')) {
      return ';/***' + substring;
    }
    return substring;
  });
  newContent = `/***${newContent}***/`;

  const ejsData = [];
  newContent = newContent.replace(/\/\*\*\*([\s\S]*?)\*\*\*\//gm, (match, subMatch) => {
    ejsData.push(subMatch);
    const whiteSpaces = subMatch.replace(/\S/g, '');
    return `/***${ejsData.length - 1}${whiteSpaces}***/`;
  });

  const outFile = path.join(options.outTs, pathInDir);
  const outMapFile = path.join(options.outTsMap, pathInDir);

  await ensureDir(outFile);

  await fs.writeFile(outFile + '.ts', [tsContent, '{', newContent, '}'].join(os.EOL), 'utf8');
  await fs.writeFile(outMapFile + '.ts.map', JSON.stringify(ejsData), 'utf8');
}

async function genEjs() {
  options.inTsMap = options.inTsMap || options.in;
  options.outEjs = options.outEjs || options.in;

  const files = await glob(path.join(options.in, '**/*.ets.js'));

  for (const file of files) {
    await transpileFileEjs(file);
  }
}

async function transpileFileEjs(file) {
  const pathInDir = path.relative(options.in, file);

  const content = await fs.readFile(file, 'utf8');
  const mapFile = path.join(options.inTsMap, pathInDir.replace('.ets.js', '.ets.ts.map'));
  const map = JSON.parse(await fs.readFile(mapFile), 'utf8');

  const beginningIndex = content.indexOf('/***');
  const endIndex = content.lastIndexOf('***/');

  let newContent = content.substring(beginningIndex, endIndex + 4);

  newContent = newContent.replace(/\/\*\*\*([\s\S]*?)\*\*\*\//gm, (match, subMatch) => {
    const mapIndex = +subMatch;
    return map[mapIndex];
  });

  await fs.writeFile(path.join(options.outEjs, pathInDir.replace('.ets.js', '.ejs')), newContent, 'utf8');
}

async function cleanUp() {
  const tsFiles = await glob(path.join(options.in, '**/*.ets.ts'));
  const mapFiles = await glob(path.join(options.in, '**/*.ets.ts.map'));
  const jsFiles = await glob(path.join(options.in, '**/*.ets.js'));

  for (const file of tsFiles.concat(mapFiles, jsFiles)) {
    await fs.unlink(file);
  }
}

if (execute) {
  execute().catch(err => {
    console.log(err);
    process.exit(1);
  });
} else {
  program.help();
}
