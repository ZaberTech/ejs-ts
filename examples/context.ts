export interface User {
  firstName: string;
  lastName: string;
}

export interface Context {
  users: User[];
  currentDate: string;
}
