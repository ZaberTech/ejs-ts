import * as ejs from 'ejs';
import { promises as fs } from 'fs';
import * as path from 'path';

import { Context } from './context';

async function main() {
  const context: Context = {
    users: [{
      firstName: 'Nick',
      lastName: 'Li',
    }, {
      firstName: 'April',
      lastName: 'Bruce',
    }, {
      firstName: 'Graeme',
      lastName: 'Potter',
    }],
    currentDate: '2019-06-07',
  };
  const compiled = await ejs.renderFile(path.join(__dirname, 'users.ejs'), context);
  await fs.writeFile('report.txt', compiled, 'utf8');
}

main().catch(err => {
  console.log(err);
  process.exit(1);
});
