module.exports = {
  "env": {
    "es6": true,
    "node": true
  },
  "extends": "eslint:recommended",
  "parserOptions": {
    "ecmaVersion": 2018
  },
  "rules": {
    "no-console": [
      "off"
    ],
    "indent": [
      "error",
      2
    ],
    "quotes": [
      "error",
      "single"
    ],
    "semi": [
      "error",
      "always"
    ],
    "camelcase": [
      "error"
    ],
    "prefer-const": [
      "error"
    ],
    "object-curly-spacing": [
      "error",
      "always"
    ],
    "arrow-parens": [
      "error",
      "as-needed",
    ],
    "prefer-arrow-callback": [
      "error"
    ],
    "no-cond-assign": [
      "error",
      "except-parens"
    ],
  }
};