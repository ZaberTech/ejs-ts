#!/bin/bash

set -e

# generate TypeScript from ETS templates
npx ejs-ts gen-ts --in examples/

# compile TypeScript (replace with your own build)
npx tsc examples/*.ts --target ES6 --module None

# generate EJS from compiled TypeScript
npx ejs-ts gen-ejs --in examples/

# Clean up by-products of the process
npx ejs-ts clean-up --in examples/

# execute JavaScript (replace with your own execution)
node examples/render.js
